# Fizz-Buzz App Test

## Context

This app provides two screens:

A form to input a limit for number generation and two numbers int1, int2 and two strings str1, str2.
A result screen that shows a list of generated number.
- The multiple of int1 are replaced with str1.
- The multiple of int2 are replaced with str2.
- The multiple of int2*int1 are replaced with str1str2. 

## Architecture

The app is coded in Kotlin, Compose for the ui screens.
It follows the clean architecture & MVVM pattern for the presentation layer.

The form and result screen are activities for simplicity.

The form screen verify the input number and string within the viewModel.
limit need to be positive
int1<int2
str1 and str2 can't be empty

The process button is disabled until the input values are valid.


The result screen take the input params and call the use case that will generate the list regarding the given params.
The list is a flow of int.
A repository generate a list from 1 to limit
The useCase on the flow the numbers to respect the algorithm constraints.
The viewModel consume the flow and show them on the flow to the screen.
For performance purpose a delay is added between flow items so that the main thread will not be blocked only for number display

The useCase is tested with a sample example -> GenerateResultUseCaseTest

## Possible Improvement

- Improve design
- Add error message per input to explain the exact constraints
- Improve test coverage
- Add logo to the App
- Use single activity for the App
- Add resilience message