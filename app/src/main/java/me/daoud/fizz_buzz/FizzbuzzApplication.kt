package me.daoud.fizz_buzz

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class FizzbuzzApplication : Application()