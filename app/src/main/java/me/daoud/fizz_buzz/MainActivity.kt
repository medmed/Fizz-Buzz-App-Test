package me.daoud.fizz_buzz

import android.content.Intent
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import dagger.hilt.android.AndroidEntryPoint
import me.daoud.fizz_buzz.ResultActivity.Companion.INT1_KEY
import me.daoud.fizz_buzz.ResultActivity.Companion.INT2_KEY
import me.daoud.fizz_buzz.ResultActivity.Companion.LIMIT_KEY
import me.daoud.fizz_buzz.ResultActivity.Companion.STR1_KEY
import me.daoud.fizz_buzz.ResultActivity.Companion.STR2_KEY
import me.daoud.fizz_buzz.form.Form
import me.daoud.fizz_buzz.form.FormViewModel
import me.daoud.fizz_buzz.ui.theme.FizzbuzzTheme

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            val viewModel: FormViewModel = hiltViewModel()

            val state by viewModel.state.collectAsState()

            FizzbuzzTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    Form(
                        int1 = state.int1,
                        setInt1 = viewModel::setInt1,
                        int2 = state.int2,
                        setInt2 = viewModel::setInt2,
                        limit = state.limit,
                        setLimit = viewModel::setLimit,
                        str1 = state.str1,
                        setStr1 = viewModel::setStr1,
                        str2 = state.str2,
                        setStr2 = viewModel::setStr2,
                        isValid = state.isValid,
                        onSendClicked = {
                            startActivity(Intent(this, ResultActivity::class.java).apply {
                                putExtra(LIMIT_KEY, state.limit)
                                putExtra(INT1_KEY, state.int1)
                                putExtra(INT2_KEY, state.int2)
                                putExtra(STR1_KEY, state.str1)
                                putExtra(STR2_KEY, state.str2)
                            })
                        }
                    )
                }
            }
        }
    }
}