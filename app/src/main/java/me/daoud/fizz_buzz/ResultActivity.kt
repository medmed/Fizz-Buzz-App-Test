package me.daoud.fizz_buzz

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.hilt.navigation.compose.hiltViewModel
import dagger.hilt.android.AndroidEntryPoint
import me.daoud.fizz_buzz.form.Form
import me.daoud.fizz_buzz.form.FormViewModel
import me.daoud.fizz_buzz.result.ui.ResultList
import me.daoud.fizz_buzz.result.ui.ResultViewModel
import me.daoud.fizz_buzz.ui.theme.FizzbuzzTheme

@AndroidEntryPoint
class ResultActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {

            val viewModel: ResultViewModel = hiltViewModel()

            val state by viewModel.state.collectAsState()

            FizzbuzzTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(),
                    color = MaterialTheme.colorScheme.background
                ) {
                    if (state.error) {
                        Text(
                            text = "Error",
                            color = MaterialTheme.colorScheme.error,
                            textAlign = TextAlign.Center
                        )
                    } else {
                        ResultList(list = state.list)
                    }
                }
            }
        }
    }

    companion object {
        const val LIMIT_KEY: String = "LIMIT_KEY"
        const val INT1_KEY: String = "INT1_KEY"
        const val INT2_KEY: String = "INT2_KEY"
        const val STR1_KEY: String = "STR1_KEY"
        const val STR2_KEY: String = "STR2_KEY"
    }
}