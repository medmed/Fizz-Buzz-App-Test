package me.daoud.fizz_buzz.form

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.foundation.verticalScroll
import androidx.compose.material3.Button
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.input.KeyboardType
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun Form(
    int1: String,
    setInt1: (String) -> Unit = {},
    int2: String,
    setInt2: (String) -> Unit = {},
    limit: String,
    setLimit: (String) -> Unit = {},
    str1: String,
    setStr1: (String) -> Unit = {},
    str2: String,
    setStr2: (String) -> Unit = {},
    isValid: Boolean,
    onSendClicked: () -> Unit = {}
) {
    Column(
        modifier = Modifier
            .verticalScroll(rememberScrollState())
            .padding(16.dp), verticalArrangement = Arrangement.spacedBy(8.dp)
    ) {
        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = int1,
            onValueChange = setInt1,
            label = { Text(text = "Int1") },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )

        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = int2,
            onValueChange = setInt2,
            label = { Text(text = "Int2") },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )

        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = limit,
            onValueChange = setLimit,
            label = { Text(text = "Limit") },
            keyboardOptions = KeyboardOptions(keyboardType = KeyboardType.Number)
        )

        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = str1,
            onValueChange = setStr1,
            label = { Text(text = "Str1") },
        )

        TextField(
            modifier = Modifier.fillMaxWidth(),
            value = str2,
            onValueChange = setStr2,
            label = { Text(text = "Str2") },
        )

        Button(modifier = Modifier.fillMaxWidth(), onClick = onSendClicked, enabled = isValid) {
            Text(text = "Process")
        }
    }

}

@Preview(showBackground = true)
@Composable
fun FormPreview() {
    Form(int1 = "", int2 = "", limit = "", str1 = "", str2 = "", isValid = false)
}