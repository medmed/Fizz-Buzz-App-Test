package me.daoud.fizz_buzz.form

import androidx.core.text.isDigitsOnly
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.update
import javax.inject.Inject

@HiltViewModel
class FormViewModel @Inject constructor() : ViewModel() {
    private val _uiState = MutableStateFlow(FormState(isValid = false))

    val state: StateFlow<FormState> = _uiState

    fun setInt1(int1: String) {
        _uiState.update { it.copy(int1 = int1) }
        updateFormState()
    }

    fun setInt2(int2: String) {
        _uiState.update { it.copy(int2 = int2) }
        updateFormState()

    }

    fun setLimit(limit: String) {
        _uiState.update { it.copy(limit = limit) }
        updateFormState()

    }

    fun setStr1(str1: String) {
        _uiState.update { it.copy(str1 = str1) }
        updateFormState()
    }

    fun setStr2(str2: String) {
        _uiState.update { it.copy(str2 = str2) }
        updateFormState()
    }

    private fun updateFormState() {
        _uiState.update { it.copy(isValid = verifyFormIsValid(it)) }
    }

    private fun verifyFormIsValid(state: FormState) = state.run {
        limit.isValidDigit()
                && int1.isValidDigit()
                && int2.isValidDigit()
                && int1.toInt() in 1..<int2.toInt()
                && limit.toInt() > 0
                && str1.isNotEmpty()
                && str2.isNotEmpty()
    }

    private fun String.isValidDigit() = isNotBlank() && isDigitsOnly()

    data class FormState(
        val int1: String = "",
        val int2: String = "",
        val limit: String = "",
        val str1: String = "",
        val str2: String = "",
        val isValid: Boolean
    )
}