package me.daoud.fizz_buzz.result.data

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import me.daoud.fizz_buzz.result.domain.NumberRepository

@Module
@InstallIn(SingletonComponent::class)
abstract class DataModule {
    @Binds
    abstract fun bindNumberRepository(
        numberRepository: NumberRepositoryImpl,
    ): NumberRepository
}