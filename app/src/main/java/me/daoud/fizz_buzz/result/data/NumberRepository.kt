package me.daoud.fizz_buzz.result.data

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import me.daoud.fizz_buzz.result.domain.NumberRepository
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class NumberRepositoryImpl @Inject constructor() : NumberRepository {
    override fun generateNumbers(limit: Int): Flow<Int> = (1..limit).asFlow()
}