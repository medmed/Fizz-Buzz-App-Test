package me.daoud.fizz_buzz.result.domain

import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class GenerateResultUseCase @Inject constructor(private val numberRepository: NumberRepository) {

    operator fun invoke(
        limit: Int,
        int1: Int,
        int2: Int,
        str1: String,
        str2: String,
    ) = numberRepository.generateNumbers(limit).map {
        when {
            it % (int1 * int2) == 0 -> str1 + str2
            it % int1 == 0 -> str1
            it % int2 == 0 -> str2
            else -> it.toString()
        }
    }
        .flowOn(Dispatchers.IO)

}