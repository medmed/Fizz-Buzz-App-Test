package me.daoud.fizz_buzz.result.domain

import kotlinx.coroutines.flow.Flow

interface NumberRepository {
    fun generateNumbers(limit: Int) : Flow<Int>
}