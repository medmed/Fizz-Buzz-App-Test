package me.daoud.fizz_buzz.result.ui

import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp

@Composable
fun ResultList(list: List<String>) {
    LazyColumn {
        items(list.size, key = { index -> index }) {
            Text(modifier = Modifier
                .fillMaxWidth()
                .padding(8.dp), text = list[it])
        }
    }
}

@Preview(showBackground = true)
@Composable
fun ResultListPreview() {
    ResultList(
        listOf(
            "1",
            "2",
            "fizz",
            "4",
            "buzz",
            "fizz",
            "7",
            "8",
            "fizz",
            "buzz",
            "11",
            "fizz",
            "13",
            "14",
            "fizzbuzz",
            "16"
        )
    )
}