package me.daoud.fizz_buzz.result.ui

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import me.daoud.fizz_buzz.ResultActivity
import me.daoud.fizz_buzz.result.domain.GenerateResultUseCase
import javax.inject.Inject
import kotlin.time.Duration.Companion.milliseconds

@HiltViewModel
class ResultViewModel @Inject constructor(
    savedStateHandle: SavedStateHandle,
    generateResultUseCase: GenerateResultUseCase
) :
    ViewModel() {
    private val _uiState = MutableStateFlow(ResultState())

    private val int1: String = savedStateHandle.get<String>(ResultActivity.INT1_KEY).orEmpty()
    private val int2: String = savedStateHandle.get<String>(ResultActivity.INT2_KEY).orEmpty()
    private val limit: String = savedStateHandle.get<String>(ResultActivity.LIMIT_KEY).orEmpty()
    private val str1: String = savedStateHandle.get<String>(ResultActivity.STR1_KEY).orEmpty()
    private val str2: String = savedStateHandle.get<String>(ResultActivity.STR2_KEY).orEmpty()

    val state: StateFlow<ResultState> = _uiState

    init {
        viewModelScope.launch {
            if (int1.isNotEmpty() && int2.isNotEmpty() && limit.isNotEmpty() && str1.isNotEmpty() && str2.isNotEmpty()) {
                generateResultUseCase.invoke(
                    limit = limit.toInt(),
                    int1 = int1.toInt(),
                    int2 = int2.toInt(),
                    str1,
                    str2
                )
                    .onEach { delay(20.milliseconds) }
                    .collect { result ->
                        _uiState.update { it.copy(list = it.list + result) }
                    }
            } else {
                _uiState.update { it.copy(error = true) }
            }

        }
    }

    data class ResultState(
        val list: List<String> = emptyList(),
        val error: Boolean = false
    )
}