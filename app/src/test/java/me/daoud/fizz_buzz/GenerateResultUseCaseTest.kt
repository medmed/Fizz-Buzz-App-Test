package me.daoud.fizz_buzz

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.test.runTest
import me.daoud.fizz_buzz.result.domain.GenerateResultUseCase
import me.daoud.fizz_buzz.result.domain.NumberRepository
import org.junit.Assert.assertArrayEquals
import org.junit.Test

class GenerateResultUseCaseTest {

    @Test
    fun testGenerator() = runTest {
        //Given
        val useCase = GenerateResultUseCase(object : NumberRepository {
            override fun generateNumbers(limit: Int): Flow<Int> = (1..16).asFlow()
        })

        //When
        val result = mutableListOf<String>()
        useCase.invoke(16, 3, 5, "fizz", "buzz").toList(result)

        //Then
        assertArrayEquals(
            listOf(
                "1",
                "2",
                "fizz",
                "4",
                "buzz",
                "fizz",
                "7",
                "8",
                "fizz",
                "buzz",
                "11",
                "fizz",
                "13",
                "14",
                "fizzbuzz",
                "16"
            ).toTypedArray(),
            result.toTypedArray()
        )
    }
}